﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IdentityService
{
    class MyClass : IIdentityService
    {

        public AuthenticationResult Authenticate(string userName, string password)
        {
            int index = FindIndex(userName);

            if (index >= 0 && password == passwords[index])
            {
                Dictionary<string, string> newProperties = new Dictionary<string, string>();
      
                if (!customProperties.ContainsKey(logins[index]))
                    return AuthenticationResult.Successful(logins[index], newProperties);
                else
                    return AuthenticationResult.Successful(logins[index], customProperties[logins[index]]);
            }
            else
            {
                if (index < 0)
                    return AuthenticationResult.Failed(AuthenticationError.UserNotFound);
                if (password != passwords[index])
                    return AuthenticationResult.Failed(AuthenticationError.InvalidPassword);
            }

            throw new NotImplementedException();
        }

        public RegistrationResult Register(string userName, string password, IDictionary<string, string> properties = null)
        {
            /*
            here must be name, pass and settings validation 
            but project budget is too small
            */

            // login
            if (FindIndex(userName) < 0)
                logins.Add(userName);
            else
                return RegistrationResult.Failed(RegistrationError.UserAlreadyExists);

            // password
            passwords.Add(password);

            // properties
            if (properties != null)
                AddProperties(userName, properties);

            return  RegistrationResult.Successful();

            /// if(login , pass != Normal) => not implem+
            throw new NotImplementedException();
        }

        public void SaveToJson(string pathToJsonFile, bool overwrite = false)
        {

            if (!overwrite && System.IO.File.Exists(pathToJsonFile))
                throw new ArgumentException();

            string json = "{";

            json += "\"logins\": ";
            json += JsonConvert.SerializeObject(logins);
            json += ",";

            json += "\"passwords\": ";
            json += JsonConvert.SerializeObject(passwords);
            json += ",";

            json += "\"customProperties\": ";
            json += JsonConvert.SerializeObject(customProperties);

            json += "}";

            System.IO.File.WriteAllText(pathToJsonFile, json);

            return;

            throw new NotImplementedException();

        }

        public MyClass()
        {
            // do nothing
        }

        public MyClass(List<string> log, List<string> pas, Dictionary<string, Dictionary<string, string>> prop)
        {
            logins = log;
            passwords = pas;
            customProperties = prop;
        }

        // ------------------

        // returns index in list by name
        private int FindIndex(string userName)
        {
            for (int i = 0; i < logins.Count; i++)
                if (String.Equals(logins[i], userName, StringComparison.CurrentCultureIgnoreCase))
                    return i; // if found => return index
            
            // if not found
            return -1;
        }
        // add properties to user
        private void AddProperties(string userName, IDictionary<string, string> properties)
        {
            Dictionary<string, string> newProperties = new Dictionary<string, string>();

            if (!customProperties.ContainsKey(userName))
            {
                // always when registration
                customProperties.Add(userName, newProperties);
                foreach (var x in properties)
                    customProperties[userName].Add(x.Key, x.Value);
            }
            else 
                // we also can add it later
                foreach (var x in properties)
                    customProperties[userName].Add(x.Key, x.Value);
        }

        // ------------------

        private List<string> logins = new List<string>(1);
        private List<string> passwords = new List<string>(1);
        /* hash table where 
         key   <- userName
         value <- list of settings
        */
        private Dictionary<string, Dictionary<string, string>> customProperties = new Dictionary<string, Dictionary<string, string>>(1);
       
        // ------------------
    }
    class Program
    {
        static int Main(string[] str)
        {
            return 0;
        }
    }
}
