﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IdentityService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace IdentityService
{
    public static class IdentityServiceFactory
    {
        public static IIdentityService CreateFromJson(string pathToJsonFile)
        {
            // Todo 
            var str = System.IO.File.ReadAllText(pathToJsonFile);
            
            var obj = JsonConvert.DeserializeObject<Dictionary<string, object>>(str);

            List<string> logins = new List<string>(1);
            List<string> passwords = new List<string>(1);
            Dictionary<string, Dictionary<string, string>> customProperties = new Dictionary<string, Dictionary<string, string>>(1);

            foreach (var x in obj)
            {
                if (x.Key == "logins")
                    logins = JsonConvert.DeserializeObject<List<string>>(x.Value.ToString());
                if (x.Key == "passwords")
                    passwords = JsonConvert.DeserializeObject<List<string>>(x.Value.ToString());
                if (x.Key == "customProperties")
                    customProperties = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(x.Value.ToString());
            }

            MyClass newClass = new MyClass(logins, passwords, customProperties);

            return newClass;

            throw new NotImplementedException();
        }

        public static IIdentityService CreateFromMemory(IEnumerable<string> users, IEnumerable<string> passwords)
        {
            // Todo
            MyClass myclass = new MyClass();

            List<string> uList = users.ToList();
            List<string> pList = passwords.ToList();

            for(int i = 0; i < uList.Count; i++)
            {
                myclass.Register(uList[i], pList[i]);
            }

            return myclass;

            throw new NotImplementedException();
        }
    }
}